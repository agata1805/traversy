// Classes
class Person {
    constructor(firstName, lastName, dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = new Date(dob);

    }
    greeting() {
        return `Hello ${this.firstName} ${this.lastName}`;
    }

    calculateAge() {
        const diff = Date.now() - this.birthday.getTime();
        const ageDate = new Date(diff);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    getsMarried(newLastName) {
    this.lastName = newLastName;
    }

    static addNumbers(x, y) {
        return x + y;
    }
}

console.log(Person.addNumbers(1,3));

const mary = new Person('mary', 'williams', '11-13-1980');
mary.getsMarried('thompson');

console.log(mary.greeting());
console.log(mary.calculateAge());
console.log(mary);


// ----------------------------- //
// Subclasses

class Human {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    greeting() {
        return `Hello there ${this.firstName} ${this.lastName}`;
    }
}

class Customer extends Person {
    constructor(firstName, lastName, phone, membership) {
        super(firstName, lastName);
        this.phone = phone;
        this.membership = membership;
    }
    static  getMembershipCost() {
        return 500;
    }
}

const john = new Customer('john', 'albeda', 1234567, 'standard');
console.log(Customer.getMembershipCost());