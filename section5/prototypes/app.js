// Object.prototype
// Person.prototype

// Person constructor
function Person(firstName, lastName, dob) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = new Date(dob);
    // this.calculateAge = function () {
    //     const diff = Date.now() - this.birthday.getTime();
    //     const ageDate = new Date(diff);
    //     return Math.abs(ageDate.getUTCFullYear() - 1970);
    // }
}

// Calculate age
Person.prototype.calculateAge = function () {
    const diff = Date.now() - this.birthday.getTime();
    const ageDate = new Date(diff);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
};

// Get full name
Person.prototype.getFullName = function () {
    return `${this.firstName} ${this.lastName}`;
};

// Gets married
Person.prototype.getsMarried = function (newLastName) {
    this.lastName = newLastName;
};

const john = new Person('john', 'doe', '8-12-1976');
const mary = new Person('mary', 'johnson', 'march 20 1978');

mary.getsMarried('smith');
console.log(john);
console.log(mary.calculateAge());
console.log(mary.getFullName());
console.log(mary.hasOwnProperty('firstName'));
console.log(mary.hasOwnProperty('getFullName'));

// ------------------------ //

// Prototypal inheritance

// Greeting
Person.prototype.greeting = function () {
    return `Hello there ${this.firstName} ${this.lastName}`;
};

const amy = new Person('amy', 'kavinsky');

// Customer constructor
function Customer(firstName, lastName, phone, membership) {
    Person.call(this, firstName, lastName);
    this.phone = phone;
    this.membership = membership;
}

// Inherit the Person prototype methods
Customer.prototype = Object.create(Person.prototype);

// Make customer.prototype return Customer()
Customer.prototype.constructor = Customer;

const customer1 = new Customer('tom', 'smith', 12344321, 'standard');

// Customer greeting
Customer.prototype.greeting = function() {
    return `Hello ${this.firstName} ${this.lastName}, welcome to our company`;
};

console.log(customer1.greeting());
console.log(amy.greeting());

// -------------------------------------------- //
// Object.create

const personPrototypes = {
    greeting: function () {
        return `Hello there ${this.firstName} ${this.lastName}`;
    },
    getsMarried: function (newLastName) {
        this.lastName = newLastName;
    }
};

const maryAnn = Object.create(personPrototypes);
maryAnn.firstName = 'Mary Ann';
maryAnn.lastName = 'Williams';
maryAnn.age = 30;
maryAnn.getsMarried('thompson');


console.log(maryAnn.greeting());

const brad = Object.create(personPrototypes, {
    firstName: {value: 'brad'},
    lastName: {value: 'traversy'},
    age: {value: 36},
});

console.log(brad.greeting());


