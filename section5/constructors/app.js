// Person constructor
function Person(name, dob) {
    this.name = name;
    this.birthday = new Date(dob);
    this.calculateAge = function () {
        const diff = Date.now() - this.birthday.getTime();
        const ageDate = new Date(diff);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }
}

const brad = new Person('Brad', '5-18-1999');
const john = new Person('john');

console.log(brad.calculateAge());
console.log(john);


// Strings
const name1 = 'jeff';
const name2 = new String('jeff');

name2.foo = 'bar';

console.log(typeof name1); // string
console.log(typeof name2); // object
