document.getElementById('button').addEventListener('click', loadData);

function loadData() {
    // create an xhr object
    const xhr = new XMLHttpRequest();


    // OPEN (specify the type of request and the url or file name)
    xhr.open('GET', 'data.txt', true);
    console.log('READYSTATE', xhr.readyState);

    xhr.onload = function () {
        if (this.status === 200) {
            // console.log(this.responseText);         // better to use it instead of onreadystate
            document.getElementById('output').innerHTML = `<h1>${this.responseText}</h1>`
        }
    };

    //optional - used for spinners or loaders
    xhr.onprogress = function() {
        console.log('READYSTATE', xhr.readyState);
    };

    // xhr.onreadystatechange = function() {
    //     if (this.status === 200 && this.readyState === 4) {
    //         console.log(this.responseText);
    //     }
    // };

    xhr.send();
    // HTTP statuses:              Ready state values:
    // 200: 'OK'                   0: request not initialized
    // 403: 'Forbidden'            1: server connection established
    // 404: 'not found'            2: request received
    //                             3: processing request
    //                             4: request finished and
    //                             response is ready

    xhr.onerror = function () {
        console.log('Request error');
    };
}

document.getElementById('button1').addEventListener('click', loadCustomer);

function loadCustomer(e) {
    const xhr2 = new XMLHttpRequest();
    xhr2.open('GET', 'customer.json', true);
    xhr2.onload = function() {
        if (this.status ===200) {
            // console.log(this.responseText);
            const customer = JSON.parse(this.responseText);
            const dummyText = `
            <ul>
                <li>ID: ${customer.id}</li>
                <li>Name: ${customer.name}</li>
                <li>Phone: ${customer.phone}</li>
            </ul>
           `;
            document.getElementById('customer').innerHTML = dummyText;
        }
    };
    xhr2.send();
}

document.getElementById('button2').addEventListener('click', loadCustomers);

function loadCustomers(e) {
    const xhr3 = new XMLHttpRequest();
    xhr3.open('GET', 'customers.json', true);
    xhr3.onload = function() {
        if (this.status ===200) {
            // console.log(this.responseText);
            const customers = JSON.parse(this.responseText);

            let output = '';
            customers.forEach(function(customer) {
                output += `
            <ul>
                <li>ID: ${customer.id}</li>
                <li>Name: ${customer.name}</li>
                <li>Phone: ${customer.phone}</li>
            </ul>
           `;
            });
            document.getElementById('customers').innerHTML = output;
        }
    };
    xhr3.send();

}
