/* Easy HTTP library
* @version 2.0.0
* @author Agata
* @license blah
 */

// class EasyHTTP {
//     // make HTTP get request
//     get(url) {
//         fetch(url)
//             .then(res => res.json())
//             .then(data => console.log(data))
//             .catch(err => console.log(err))
//     }
// }
class EasyHTTP {
    // make HTTP GET request
    async get(url) {
        const response = await fetch(url);
        const resData = await response.json();
        return resData;
    }

    // make an HTTP POST request
    async post(url, data) {
          const response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(data)
            });
          const resData = await response.json();
          return resData;
    }

    // make an HTTP PUT request
    async put(url, data) {
            const response = await fetch(url, {
                method: 'PUT',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(data)
            });
                const resData = await response.json();
                return resData;
    }

    // make an HTTP DELETE request
    async delete(url) {
           const response = await fetch(url, {
                method: 'DELETE',
                headers: {
                    'Content-type': 'application/json'
                }
            });
        const resData = await ("Resource deleted");
        return resData;
    }
}