// async function myFunc() {
//     const promise = new Promise((resolve, reject) => {
//         setTimeout(() => resolve('Hello'), 1000)
//     });
//
//     const error = false;
//     if (!error) {
//         const res = await promise; // wait until the promise is resolved (1s)
//         return res;
//     } else {
//         await Promise.reject(new Error('Something went wrong'));
//     }
// }
//
// // putting async in front of a function, makes this function return a promise
// myFunc()
//     .then(res => console.log(res))
// .catch(err => console.log(err));


async function getUsers() {
    // await response of the fetch call
    const response = await fetch('https://jsonplaceholder.typicode.com/users');

    // only proceed when it's resolved
    const data = await response.json();

    // only proceed once the second promise is resolved
    return data;
}

getUsers()
.then(users => console.log(users));
