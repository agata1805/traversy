# Introduction
***
This repository was created to make notes and follow Brad Traversy coding in JS course. It consists of 6 sections (including different projects) and one bigger project - booklist.

# Topics covered
***
* DOM manipulation tasks
* Object oriented JavaScript
* Booklist project:
    - UI manipulations
    - validation and messages alerts
    - ES6 classes
    - Local Storage
* asynchronous JS
* Ajax and fetch API
* Github Finder project
* Error handling
* Regular Expressions

# Languages/Frameworkd/Libraries
***
* HTML
* CSS 
* JavaScript
* Skeleton

